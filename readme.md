# GIT_Basics
### Install GIT on your workstation
<img width="263" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/0f8e7f5f-cf45-4de4-a191-38484d19106a">

### Set git global configs name and email
<img width="368" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/2f8e4b39-93c7-4989-9f2f-fa44990d4b8a">

###  Created account on GitLab
<img width="944" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/77773035-55b0-4610-ba87-8381b5387683">

### Generated ssh-key and integrated it with GitLab
<img width="446" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/f4a593f3-3be4-4865-ac37-455b7e694db6">
<img width="536" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/7ec2d54c-69cd-4eb1-a826-4f8e6b3c325a">

### Created new project on GitLab
<img width="959" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/4d98301d-59d9-485a-81c0-790001f925d3">

### Cloned project on my workstation
<img width="403" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/db5397cd-6229-4bf3-bf62-68eff821a1c2">
<img width="173" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/9f2ee834-0c3b-48b7-b305-bbee7a6fd410">

### Created git ignore file
<img width="960" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/daef7798-3b55-45ce-b627-bfcb5fe06d98">

### Opened git console in root directory
<img width="287" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/d8e45f0d-68e5-472f-bc30-225e774877ee">

### Created directory  “task1 – git pracrice I”
<img width="511" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/d40205b9-157e-45e4-b54d-40c05b488d84">

### Created empty readme.txt file
<img width="633" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/0dcf1fea-869d-43e9-a0e4-96adba5c3f40">

### Made init commit
<img width="737" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/4d86b46f-db06-4925-b769-e47cdc9a5343">

### Created develop branch and checkout on it
<img width="665" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/7f7c5c02-2bae-42d7-9638-5068abb51967">

### Created index.html empty file. Commited
<img width="695" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/bb7066b8-974c-42b6-bbed-bdb846f8e7aa">

### Created branch with name “images”. Checkout on it. Added images folder with some images inside it. Commited
<img width="701" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/ddaaadf8-a323-4c31-a699-5453edf7873a">

### Changed index.html. Added images source inside it. Commited.
<img width="809" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/5037feed-979c-4d59-bc91-035074200a6b">

### Went back to develop branch
<img width="660" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/5832439e-b73e-485c-9f3a-aad0b0688a24">

### Created branch with name “styles”. Checkout on it. Added styles folder with styles source inside it. Commited.
<img width="658" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/df5054a3-6567-4a43-96f6-ed24857e677f">
<img width="725" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/83dc2d8a-69c9-4361-92f6-36d6cf494fc6">

### Changed index.html. Commited
<img width="607" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/bd6a93a7-6c4b-49cc-a1ab-36db0e3af1c1">

### Went back to develop branch
<img width="682" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/f2218184-7338-4caa-bfd5-219f60b6bcc8">

### Merged “images” into “develop”
<img width="673" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/b1cf6898-c183-4c6b-8c16-3784152a0377">

### Merge “styles” into “develop”
<img width="791" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/381259c9-912b-46a4-bebe-a88f57964de1">
<img width="731" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/39bb8181-ebd6-466e-9d4d-c5a92171444c">
<img width="737" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/fb2ca092-df5d-4022-affe-11594e2bf7f5">

### Merged develop into master
<img width="665" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/d0152e12-2150-4316-b612-9338d9923b0f">

### Went to develop branch and repeat items once more (used names “images2”, “styles2” for new branches). Used git rebase command instead of git merge.
<img width="716" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/cd7799d9-bf0f-4e38-accd-8c09d0a7abc0">
<img width="716" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/d4d7bf6e-0cc4-486a-9384-a94de427e96a">
<img width="695" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/9149ff0c-9170-40f6-962a-bd0fc06964c1">

### Merged develop into master
<img width="672" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/c4bf73cf-2621-4ee3-9f6c-ee2ebb3f20da">
<img width="722" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/f2205150-a669-4e2f-bac6-74ccc40fc44f">

### Pushed all your changes
<img width="944" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/92b1081e-d315-4188-b47d-0b7cb5a4c512">
<img width="947" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/1fe97e06-f441-4480-82f9-a17b510f1906">

### Git reflog
<img width="943" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/3c018fbf-4f01-4b7c-b5e9-cda0233cb3f7">
<img width="953" alt="image" src="https://github.com/dimapovarchuk/GIT_Basics/assets/52627259/b4a834e1-e143-4649-b638-89b2d1e86c35">






